package helper

import "reflect"

//Max function
func Max(list interface{}) interface{} {

	itemsValue := reflect.ValueOf(list)
	listType := itemsValue.Type()
	listKind := listType.Kind()

	if listKind != reflect.Array && listKind != reflect.Slice {
		panic("Đầu vào không hợp lệ")
	}
	elementType := listType.Elem()
	elementKind := elementType.Kind()

	max := itemsValue.Index(0)
	for i := 1; i < itemsValue.Len(); i++ {
		switch elementKind {
		case reflect.Int, reflect.Int32:
			if max.Int() < itemsValue.Index(i).Int() {
				max = itemsValue.Index(i)
			}
		}
	}
	return max.Interface()
}
