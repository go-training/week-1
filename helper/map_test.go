package helper

import (
	"reflect"
	"testing"
)

func rectangle(x int) float32 {
	return float32(x) * float32(x)
}

func TestMap(t *testing.T) {
	input := []int{4, 8}
	expect := []int{16, 64}
	actual := Map(input, rectangle)
	print(actual)
	if reflect.DeepEqual(actual, expect) == true {
		t.Errorf("expect: %v, but actual %v", actual, expect)
	}
}
