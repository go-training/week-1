package helper

import (
	"reflect"
	"testing"
)

func TestMax(t *testing.T) {

	arra := []int{1, 2, 3, 4, 5, 6, 7, 8, 19, 10}
	arrb := []int{22, 33, 44, 8, 19, 10}

	type args struct {
		list interface{}
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		// TODO: Add test cases.
		{
			name: "test case 1",
			args: args{
				list: arra,
			},
			want: 19,
		},
		{
			name: "test case 2",
			args: args{
				list: arrb,
			},
			want: 44,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Max(tt.args.list); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Max() = %v, want %v", got, tt.want)
			}
		})
	}
}
